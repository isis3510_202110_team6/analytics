const { useBabelRc, override } = require('customize-cra')

module.exports = override(
  // Recommended configuration from material-ui to improve bundle size
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useBabelRc()
);