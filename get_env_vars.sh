chosen=$1
if [ -z ${chosen+x} ]; then
  environment=$chosen
else
  echo "Did not specify environment, assuming current branch"
  current=$(git rev-parse --abbrev-ref HEAD)
  valid="test staging prod"
  if [[ ! " ${valid[@]} " =~ " ${current} " ]]; then
    echo "$current is not a valid environment, assuming test"
    environment="test"
  else
    echo "$current is a valid environment"
    environment=$current
  fi
fi

echo "Setting environment variables based on environment: ${environment}"
aws s3 cp s3://cleverlynk-vars/cleverlynk_web/$environment ./.env