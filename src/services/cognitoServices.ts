import { Auth } from 'aws-amplify';

export interface ISignUpInfo {
  email: string;
  password: string;
  passwordConfirmation: string;
  name: string;
}

export interface IConfirmInfo {
  email: string;
  code: string;
}

export interface ISignInInfo {
  username: string;
  password: string;
}

export interface IAnswer {
  success: boolean;
  message: string;
  payload?: any;
}

export async function authSignUp(values: ISignUpInfo): Promise<IAnswer> {

  try {
    const { user } = await Auth.signUp({
      username: values.email,
      password: values.password,
      attributes: {
        name: values.name,          // optional
      }
    });
    
    return {
      success: true,
      message: "El usuario se ha registrado con éxito, falta confirmar el registro.",
      payload: user
    }
  } catch (error) {
    console.error(error)
    return {
      success: false,
      payload: error,
      message: "El registro del usuario falló"
    }
  }
}

export async function checkIsAuthenticated(): Promise<IAnswer> {
  try {
    const user = await Auth.currentAuthenticatedUser();
    
    return {
      success: true,
      message: "Hay un usuario autenticado.",
      payload: user
    }
  } catch (error) {
    console.error(error)
    return {
      success: false,
      message: "No se encontró un usuario",
      payload: error
    }
  }
}

export async function authConfirmSignUp(values: IConfirmInfo): Promise<IAnswer> {
  try {
    await Auth.confirmSignUp(values.email, values.code);
    return {
      success: true,
      message: "El registro ha sido confirmado."
    }

  } catch (error) {
    console.error(error)
    return {
      success: false,
      payload: error,
      message: "El registro no ha podido ser confirmado."
    }
  }
}


export async function authSignIn(values: ISignInInfo): Promise<IAnswer> {
  try {
    const user = await Auth.signIn(values.username, values.password);
    return {
      success: true,
      message: `Se ha iniciado la sesión con éxito`,
      payload: user
    }
  } catch (error) {
    console.error('error signing in', error);
    return {
      success: false, 
      payload: error,
      message: "No se ha podido iniciar la sesión."
    }
  }
}

export async function authSignOut(): Promise<IAnswer> {
  try {
    await Auth.signOut();
    
    return {
      success: true,
      message: "Ha salido de la sesión"
    }
  } catch (error) {
    console.error('error signing out: ', error);
    return {
      success: false, payload: error,
      message: "No se ha podido cerrar sesión."
    }
  }
}