import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import "bootstrap/dist/css/bootstrap.min.css";
import CognitoProvider from './contexts/CognitoContext';
import Amplify, { Auth } from "aws-amplify";
import awsconfig from "./aws-exports";
//@ts-ignore
awsconfig["graphql_headers"] = async () => {
  return { Authorization: (await Auth.currentSession()).getIdToken().getJwtToken() };
};
Amplify.configure(awsconfig);
Amplify.configure({ Auth: { authenticationFlowType: "USER_PASSWORD_AUTH" } });


ReactDOM.render(
  <React.StrictMode>
    <CognitoProvider>
    <App />
    </CognitoProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
