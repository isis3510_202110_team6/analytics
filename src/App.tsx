import React, { lazy, Suspense } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import FallbackClynk from "./pages/Tools/FallbackClynk";
import PrivateRoute from "./pages/Tools/PrivateRoute";

const Login = lazy(() => import("./pages/login/Login"));
const Analytics = lazy(() => import("./pages/Analytics/Analytics"));

const colorAdjust = (color: string, amount: number) => {
  return (
    "#" +
    color
      .replace(/^#/, "")
      .replace(/../g, color => ("0" + Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)).substr(-2))
  );
};

const createThemeColor = () => {
  return createMuiTheme({
    typography: {
      fontFamily: "Inter",
    },
    palette: {
      primary: {
        main: "#1d2c41",
      },
      secondary: {
        main: colorAdjust("#1d2c41", 60),
      },
      warning: {
        main: "#ffbf00",
      },
      error: {
        main: "#eb4034",
      },
      success: {
        main: "#76A30F",
      },
    },
  });
};

function App() {
  return (
    <ThemeProvider theme={createThemeColor()}>
      <Router>
        <Suspense fallback={<FallbackClynk />}>
          <Switch>
            {/*TODO HOME*/}
            <Route path="/" exact component={Login} />
            <PrivateRoute path="/analytics" exact component={Analytics}/>
          </Switch>
        </Suspense>
      </Router>
    </ThemeProvider>
  );
}

export default App;
