import { Auth } from "aws-amplify";
import React, { useState, useEffect } from "react";
import { PFXPermissions} from "../models/Interfaces";
import {
  checkIsAuthenticated,
  authSignUp,
  authSignIn,
  authSignOut,
  ISignInInfo,
  IAnswer,
  ISignUpInfo,
  IConfirmInfo,
  authConfirmSignUp,
} from "../services/cognitoServices";

interface IAllowedActions extends PFXPermissions {
  companyId: string;
  isOwner: boolean;
}

export interface ICognitoContext {
  isAuthenticated: boolean;
  user: any;
  isLoading: boolean;
  isLoggedIn: boolean;
  signUp: (credentials: ISignUpInfo) => void;
  signIn: (credentials: ISignInInfo, setError: Function, setLoading: Function) => void;
  confirmSignUp: (credentials: IConfirmInfo) => void;
  changePassword: (newPassword: string, cbk: Function) => void;
  company: string | undefined;
  signOut: () => void;
  allowedActions: IAllowedActions | undefined;
}
export const CognitoContext = React.createContext<ICognitoContext | undefined>(undefined);

export default function CognitoProvider(props: { children: React.ReactNode }) {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [user, setUser] = useState<any>(undefined);
  const [company, setCompany] = useState<string | undefined>(undefined);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [allowedActions, setAllowedActions] = useState<IAllowedActions | undefined>(undefined);
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);

  useEffect(() => {
    checkAuth();
  }, []);

  useEffect(() => {
    if (user && user !== "The user is not authenticated" && user.challengeName !== "NEW_PASSWORD_REQUIRED") {
      setIsLoggedIn(true);
    } else if (!user) {
      setIsLoggedIn(false);
    }
  }, [user]);

  const checkAuth = () => {
    setIsLoading(true);
    checkIsAuthenticated()
      .then((answer: IAnswer) => {
        setIsAuthenticated(answer.success);
        setUser(answer.payload);
        if (answer.payload.challengeName === undefined) {
          let parsedAllowedActions = JSON.parse(answer.payload.signInUserSession.idToken.payload.dynamoUser);
          setCompany(parsedAllowedActions.companyId);
          setAllowedActions(parsedAllowedActions);
        }
      })
      .catch(() => setIsAuthenticated(false))
      .finally(() => setIsLoading(false));
  };

  const signIn = (credentials: ISignInInfo, setError: Function, setLoading: Function) => {
    setLoading(true);
    authSignIn(credentials)
      .then((answer: IAnswer) => {
        if (answer.success) {
          setUser(answer.payload);
        }
        setIsAuthenticated(answer.success);
        setError(!answer.success, answer.payload);
        if (answer.success) {
          let parsedAllowedActions = JSON.parse(answer.payload.signInUserSession.idToken.payload.dynamoUser);
          setCompany(parsedAllowedActions.companyId);
          setAllowedActions(parsedAllowedActions);
        }
      })
      .catch(error => {
        setIsAuthenticated(false);
      })
      .finally(() => setLoading(false));
  };

  const changePassword = (newPassword: string, cbk: Function) => {
    setIsAuthenticated(false);
    Auth.completeNewPassword(user, newPassword)
      .then(user => {
        setIsAuthenticated(true);
        setUser({ ...user });

        let parsedAllowedActions = JSON.parse(user.payload.signInUserSession.idToken.payload.dynamoUser);
        setCompany(parsedAllowedActions.companyId);
        setAllowedActions(parsedAllowedActions);
        // window.location.reload();
      })
      .catch(e => {
        console.error(e);
      });
  };

  const signOut = () => {
    setIsLoading(true);
    authSignOut()
      .then((answer: IAnswer) => {
        if (answer.success) {
          setIsAuthenticated(false);
          setUser(undefined);
        } else {
          console.error(answer.message);
        }
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => setIsLoading(false));
  };

  const signUp = (credentials: ISignUpInfo) => {
    setIsLoading(true);
    authSignUp(credentials)
      .then((answer: IAnswer) => {
        if (answer.success) {
          setIsAuthenticated(true);
          setUser(answer.payload);
        }
      })
      .catch(error => {
        console.error(error);
        setIsAuthenticated(false);
      })
      .finally(() => setIsLoading(false));
  };

  const confirmSignUp = (credentials: IConfirmInfo) => {
    setIsLoading(true);
    authConfirmSignUp(credentials).then((answer: IAnswer) => {
      if (answer.success) {
      }
    });
  };

  return (
    <CognitoContext.Provider
      value={{
        user,
        isLoggedIn,
        company,
        isAuthenticated,
        isLoading,
        allowedActions,
        signIn,
        signOut,
        signUp,
        confirmSignUp,
        changePassword,
      }}>
      {props.children}
    </CognitoContext.Provider>
  );
}

export const useCognito = () => React.useContext(CognitoContext);
