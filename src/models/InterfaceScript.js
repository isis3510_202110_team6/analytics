const fs = require("fs");

const mutationsMap = {
  Float: "number",
  Int: "number",
  ID: "string",
  AWSPhone: "string",
  String: "string",
  AWSEmail: "string",
  AWSURL: "string",
  Boolean: "boolean",
  AWSJSON: "any",
  AWSDateTime: "string",
  AWSDate: "string",
  AWSTime: "string",
};

function InterfaceScript() {
  try {
    fs.unlinkSync("./Interfaces.ts");
  } catch (e) {}
  let sb = [];
  fs.readFile("../../schema.graphql", "utf-8", (err, data) => {
    let i = 0;
    let array = data.split("\n");
    // console.log(array);
    while (i < array.length) {
      if (
        array[i] !== undefined &&
        array[i].indexOf("PFX") !== -1 &&
        array[i].split(" ")[0] === "type" &&
        array[i].indexOf("{") !== -1 &&
        array[i].split(" ")[1] !== "Mutation" &&
        array[i].split(" ")[1] !== "Query" &&
        array[i].split(" ")[1] !== "Subscription"
      ) {
        let typeName = array[i].split(" ")[1];
        sb.push(`export interface ${typeName} {\n`);
        sb.push(`__typename?: string; \n`);
        ++i;
        console.log(array[i]);
        while (array[i].replace("\r", "") !== "}") {
          if (array[i].indexOf("(") !== -1) {
            let name = array[i].split("(")[0];
            // console.log(`[${array[i]}]`, "------>",  typeConverter(`${array[i].split(")")[1].replace(": ", "")}[] | undefined`).trim());
            // let type = typeConverter(`[${array[i].split(":")[1]}]`).trim();
            console.log(array[i]);
            let type = `${array[i].split(")")[1].replace(": ", "")}`;
            sb.push(`${name}?: ${type}; \n`);
            i++;
          } else {
            sb.push(
              array[i].split(":")[0] + "?: " + typeConverter(array[i].split(":")[1].replace("\r", "")).trim() + ";\n"
            );
            i++;
          }
        }
        sb.push("}\n\n");
      } else if (array[i] !== undefined && array[i].split(" ")[0] === "enum" && array[i].indexOf("{") !== -1) {
        sb.push(`export enum ${array[i].split(" ")[1].trim()} { \n`);
        i++;
        while (array[i].replace("\r", "") !== "}") {
          sb.push(`${array[i].trim()} = '${array[i].trim()}', \n`);
          i++;
        }
        sb.push("}\n\n");
      }
      i++;
    }
    fs.writeFile("./Interfaces.ts", sb.join(""), function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    });
  });
}

function generateBrackets(n) {
  let brackets = "[]";
  let ans = "";
  for (let i = 0; i < n; i++) {
    ans += brackets;
  }
  return ans;
}

function generateBrackets(n){
	let brackets = "[]";
	let ans = "";
	for(let i = 0 ; i < n; i++){
		ans+=brackets;
	}
	return ans;
}

function typeConverter(input) {
  input = input.trim();
  let length = input.split("[").length;
  if (length > 1) {
    let closingBracketIndex = input.indexOf("]");
    return `${typeConverter(input.substring(length - 1, closingBracketIndex))}${generateBrackets(length - 1)}`;
  }
  // if(input.startsWith("[[")){
  // 	let closingBracketIndex = input.indexOf(']');
  // 	return `${typeConverter(input.substring(2, closingBracketIndex))}[][]`;
  // }
  // else if (input.startsWith("[")) {
  // 	let closingBracketIndex = input.indexOf(']');
  // 	return `${typeConverter(input.substring(1, closingBracketIndex))}[]`;
  // }
  input = input.replace("!", "");
  return mutationsMap[input] !== undefined ? `${mutationsMap[input]}` : `${input}`;
}

InterfaceScript();
