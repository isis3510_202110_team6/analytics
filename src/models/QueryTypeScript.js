const fs = require("fs");

function InterfaceScript() {
  const necessaryTypes = [
    "CheckCredentialsQuery",
    "QGetUserQuery",
    "QGetItemQuery",
    "QGetCleverlynkQuery",
    "QGetTemplateQuery",
    "QGetSkuQuery",
    "QGetCompanyQuery",
    "GetBillingContactQuery",
    "GetWompiPsInfoQuery",
    "QListCleverlynksQuery",
    "QListOrdersQuery",
    "QListCompanysQuery",
    "QGetLogisticsInfoQuery",
    "GetDashboardBillingDataQuery",
    "ListInvoicesByCompanyQuery",
    "GetCardInfoQuery",
    "MCreateTemplateMutation",
    "MCreateCleverlynkGroupMutation",
    "MUpdateCleverlynkGroupMutation",
    "PFXWompiUpsertCardInput",
    "PFXWompiUpsertNequiInput",
    "UpsertWompiCardPsMutation",
    "UpsertWompiNequiPsMutation",
    "SaveMpCredsMutation",
    "ListCatalogsWithItemQuery",
    "ListCatalogsWithSubItemQuery",
    "PropagateItemInCatalogMutation",
    "ListUsernamesByCompanyQuery",
  ];

  let sb = [];
  fs.readFile("../API.ts", "utf-8", (err, data) => {
    let i = 0;
    let array = data.split("\n");
    while (i < array.length) {
      const line = array[i];
      if (line.includes("export")) {
        const keyWord = line.split(" ")[2];
        let a = necessaryTypes.find(s => keyWord.toUpperCase() === s.toUpperCase());
        if (a) {
          array[i] = array[i].replace(keyWord, a);
          while (!array[i].includes("};")) {
            sb.push(array[i]);
            i++;
          }
          sb.push(array[i]);
        }
      }
      i++;
    }

    fs.appendFile("./Interfaces.ts", sb.join("\n"), function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    });
  });
}

InterfaceScript();
