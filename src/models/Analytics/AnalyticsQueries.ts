import { GraphQLResult } from "@aws-amplify/api";
import { PFXCleverlynk, PFXOrder, PFXCompany, QGetUserQuery } from "../Interfaces";

export interface ICleverlynksOrdersResult {
  orders: PFXOrder[];
  lastEvaluatedKey: string | null;
  createdAt: string | null;
}

export interface IManyCleverlynksOrdersResult {
  orders: PFXOrder[];
  keys: Array<
    { lastEvaluatedKey: string | undefined; cleverlynkId: string; createdAt: string; amountOrders: number } | undefined
  >;
}

export const privateAnalyticsQuery = (nextToken : any) => `
  query q {
    qListOrders(limit: 10000, nextToken: ${nextToken ? `"${nextToken}"` : null}) {
      nextToken
      items {
        createdAt
        cleverlynkId
        companyId
        paymentMethod
        orderStatus
        price {
          total
        }
        cleverlynk {
          amountRenders
        }
        company {
          name
        }
      }
    }
  }
`;

export const getAllCompanies = (nextToken :any) => `
  query q {
    qListCompanys(limit: 10000, nextToken: ${nextToken ? `"${nextToken}"` : null}) {
      nextToken
      items {
        name
        id
        createdAt
      }
    }
  }
`;

export const getAllCleverlynks = (nextToken : any) => `
  query q {
    qListCleverlynks(limit: 1000, nextToken: ${nextToken ? `"${nextToken}"` : null}) {
      nextToken
      items {
        updatedAt
        id
        name
        company {
          id
          name
        }
      }
    }
  }
`;

export const audienceBehaviorQuery = (username: any) => `
query MyQuery {
	qGetUser(userName:"${username}"){
    company{
      cleverlynks(limit:100000){
          items{
          id
          }
      }
      URLParam
    }
  }
}`;

export const ordersHistoricDataFromClynk = (
  userName : any,
  cleverlynkId : any,
  lastEvaluatedKey: string | null,
  createdAt: string | null
) => `
  query MyQuery{
    qGetUser(userName: "${userName}"){
      cleverlynks(cleverlynkId: {eq:"${cleverlynkId}"}) {
        items {
          cleverlynk {
            ordersByDateRange(
              exclusiveStartKey: ${
                lastEvaluatedKey
                  ? `{  id : "${lastEvaluatedKey}" 
                    cleverlynkId: "${cleverlynkId}"
                    createdAt: "${createdAt}"
                  }`
                  : null
              }
            ){
              lastEvaluatedKey{
                createdAt
                cleverlynkId
                id
              }
              items{
                createdAt
                price{
                  total
                }
                paymentMethod
                deliveryInfo{
                  deliveryLocation{
                    lat
                    lng
                  }
                }
                cleverlynkId
                orderStatus
                items{
                  quantity
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const ordersHistoricData = (userName: any, startDate?: any) => `
  query MyQuery{
    qGetUser(userName: "${userName}"){
      cleverlynks {
        items {
          cleverlynk {
            ordersByDateRange(startDate: ${startDate ? `"${startDate}"` : null}){
              lastEvaluatedKey{
                createdAt
                cleverlynkId
                id
              }
              items{
                createdAt
                price{
                  total
                }
                paymentMethod
                deliveryInfo {
                  deliveryLocation {
                    lat
                    lng
                  }
                }
                cleverlynkId
                orderStatus
                items{
                  quantity
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const cleverlynksHistoricData = (username : any) => `
query MyQuery {
    qGetUser(userName:"${username}"){
      company{
        cleverlynks(limit:1000000000){
          items{
            id
            updatedAt
            name
            amountRenders
          }
        }
      }
    }
  }`;

export const listCompaniesQuery = () => `
  query myQuery {
    qListCompanys(filter:{
      id:{
        ne:"a15Q1X0j82"
      }
    }){
      items{
        id
        name
        createdAt
      }
    } 
  }
`;

export function parsingAudienceBehavior(cleverlynk: GraphQLResult<QGetUserQuery>): PFXCompany {
  const data = cleverlynk.data?.qGetUser?.company;
  return {
    URLParam: data?.URLParam,
    cleverlynks: data?.cleverlynks,
  } as PFXCompany;
}

export function parsingOrdersHistoricData(ordersDetail: GraphQLResult<any>): IManyCleverlynksOrdersResult {
  const ords =
    ordersDetail.data?.qGetUser?.cleverlynks?.items
      ?.map((clynk: any) => {
        return (
          clynk?.cleverlynk.ordersByDateRange?.items?.map((order: any) => ({
            createdAt: order?.createdAt,
            price: order?.price,
            paymentMethod: order?.paymentMethod,
            cleverlynkId: order?.cleverlynkId,
            orderStatus: order?.orderStatus,
            items: order?.items,
            deliveryInfo: order?.deliveryInfo,
          })) || []
        );
      })
      .flat() || [];
  return {
    keys:
      ordersDetail.data?.qGetUser?.cleverlynks?.items?.map((s : any) => {
        return {
          amountOrders: s?.cleverlynk.ordersByDateRange?.items?.length,
          cleverlynkId: s?.cleverlynk.ordersByDateRange?.lastEvaluatedKey?.cleverlynkId,
          lastEvaluatedKey: s?.cleverlynk.ordersByDateRange?.lastEvaluatedKey?.id,
          createdAt: s?.cleverlynk.ordersByDateRange?.lastEvaluatedKey?.createdAt,
        };
      }) || [],
    orders: ords,
  };
}

export function parsingOrdersHistoricDataFromClynk(ordersDetail: GraphQLResult<any>): ICleverlynksOrdersResult {
  return {
    createdAt: ordersDetail.data?.qGetUser?.cleverlynks?.items
      ? ordersDetail.data?.qGetUser?.cleverlynks?.items[0].cleverlynk.ordersByDateRange.lastEvaluatedKey
        ? ordersDetail.data?.qGetUser?.cleverlynks?.items[0].cleverlynk.ordersByDateRange.lastEvaluatedKey.createdAt
        : null
      : null,
    lastEvaluatedKey: ordersDetail.data?.qGetUser?.cleverlynks?.items
      ? ordersDetail.data?.qGetUser?.cleverlynks?.items[0].cleverlynk.ordersByDateRange.lastEvaluatedKey
        ? ordersDetail.data?.qGetUser?.cleverlynks?.items[0].cleverlynk.ordersByDateRange.lastEvaluatedKey.id
        : null
      : null,
    orders: ordersDetail.data?.qGetUser?.cleverlynks?.items
      ? ordersDetail.data?.qGetUser?.cleverlynks?.items[0]?.cleverlynk?.ordersByDateRange?.items?.map((order : any) => {
          return {
            createdAt: order?.createdAt,
            price: order?.price,
            paymentMethod: order?.paymentMethod,
            cleverlynkId: order?.cleverlynkId,
            orderStatus: order?.orderStatus,
            items: order?.items,
            deliveryInfo: order?.deliveryInfo,
          } as PFXOrder;
        }) || []
      : [],
  };
}

export function parsingCleverlynksHistoricData(cleverlynks: GraphQLResult<QGetUserQuery>): PFXCleverlynk[] {
  const data = cleverlynks.data?.qGetUser?.company?.cleverlynks?.items;
  return (
    data?.map(
      cl =>
        ({
          id: cl?.id,
          updatedAt: cl?.updatedAt,
          name: cl?.name,
          amountRenders: cl?.amountRenders,
        } as PFXCleverlynk)
    ) || []
  );
}
