import React, { useMemo, useState } from "react";
import CleverlynkLogo from "../../images/cleverlynkLogoBlack.png";
import { Paper, Grid,  TextField, Button, Typography, CircularProgress } from "@material-ui/core";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { useCognito } from "../../contexts/CognitoContext";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    loginContainer: {
      height: "100%",
      margin: 0,
      backgroundColor: "rgb(237, 246, 245)",
    },
    loginPaper: {
      paddingBottom: 25,
      paddingLeft: 20,
      paddingRight: 20,
      borderRadius : "14px",
    },
    buttonProgress: {
      position: "absolute",
      top: "50%",
      left: "50%",
      marginTop: -12,
      marginLeft: -12,
    },
  })
);

const dictionary = {
  username: "Usuario",
  password: "Contraseña",
  login: "Iniciar Sesión",
  errorLogin: "El usuario o la contraseña se encuentran incorrectos",
};

export default function Login() {
  const classes = useStyles();
  const cognitoContext = useCognito();
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [error, setError] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const disabledButton = useMemo(() => (!username && !password) || loading || error, [username, password, loading, error]);
  const history = useHistory();
  function handleUsernameChange(ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
    setUsername(ev.target.value);
    if (error) setError(false);
  }
  function handlePasswordChange(ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
    setPassword(ev.target.value);
    if (error) setError(false);
  }
  function login() {
    setLoading(true);
    // TODO redirect to home when home is done
    Promise.resolve(cognitoContext?.signIn({ username, password }, () => setError(true), setLoading))
      .then(() => history.push({pathname:"/Analytics"}))
      .catch(err => {
        setError(true);
        setLoading(false);
        console.error(err);
      })
  }
  return (
    <Grid container className={classes.loginContainer} justify="center" alignItems="center">
      <Grid item container justify="center" alignItems="center" xs={12} lg={3} md={5}>
        <Paper className={classes.loginPaper} elevation={3}>
          <Grid container justify="center" spacing={3}>
            <Grid item xs={12} container justify="center">
              <img src={CleverlynkLogo} alt="Logo Cleverlynk" style={{ width: "90%" }} />
            </Grid>
            <Grid item xs={12} container justify="center">
              <TextField
                variant="outlined"
                size="small"
                fullWidth
                label={dictionary.username}
                onChange={ev => handleUsernameChange(ev)}
                value={username}
              />
            </Grid>
            <Grid item xs={12} container justify="center">
              <TextField
                variant="outlined"
                size="small"
                fullWidth
                label={dictionary.password}
                onChange={ev => handlePasswordChange(ev)}
                value={password}
                type="password"
                onKeyPress={ev => {
                  if (ev.key === "Enter") login();
                }}
              />
            </Grid>
            {error && (
              <Grid item xs={12}>
                <Typography color="error" variant="caption" align="center">
                  {dictionary.errorLogin}
                </Typography>
              </Grid>
            )}
            <Grid item xs={12} container justify="center">
              <Button onClick={login} variant="contained" fullWidth disabled={disabledButton} color="primary">
                {dictionary.login}
                {loading ? <CircularProgress size={24} className={classes.buttonProgress} /> : null}
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}
