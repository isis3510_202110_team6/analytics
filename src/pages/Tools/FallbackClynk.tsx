import { makeStyles, Theme, createStyles, useTheme } from '@material-ui/core/styles';
import {useMediaQuery} from "@material-ui/core";
import React from 'react';
import logo from "../../images/loader_logo.png";

interface IFallbackClynk {
  opaque? : boolean;
}

const FallbackClynk = ({opaque} : IFallbackClynk) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        backgroundColor: opaque ? "#f7f7f74c" : "#f7f7f7", 
        width: (opaque && !isMobile) ? "83%" : "100%",
        height: "100%", 
        display: "flex", justifyContent: "center", 
        alignItems: "center",
        zIndex: 999999,
        position: "fixed",
        top: 0,
      },
      pulse: {
        borderRadius: "50%",
        margin: "10px",
        boxShadow: " 0 0 0 0 rgba(0, 0, 0, 1)",
        transform: "scale(1)",
        animation: "$pulse 1.5s infinite"
      },
      "@keyframes pulse": {
        "0%": {
          transform: "scale(0.90)",
          boxShadow: "0 0 0 0 #02f7bf",
        },
        "70%": {
          transform: "scale(1)",
          boxShadow: "0 0 0 20px rgba(0, 0, 0, 0)",
        },
        "100%": {
          transform: "scale(0.90)",
          boxShadow: "0 0 0 0 rgba(0, 0, 0, 0)",
        }
      }
    }
    ));

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img id="fallback" className={classes.pulse} src={logo} height={100} width={100} alt="loader logo"/>
    </div>
  );
};
export default FallbackClynk;