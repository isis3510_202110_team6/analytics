import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useCognito } from "../../contexts/CognitoContext";
import Fallback from "./Fallback";

const PrivateRoute = ({ component, ...rest }: any) => {
  const { isLoading, isLoggedIn } = useCognito()!;

  const routeComponent = (props: any) =>
    !isLoading ? (
      isLoggedIn ? (
        React.createElement(component, props)
      ) : (
        <Redirect to={{ pathname: "/" }} />
      )
    ) : (
      <Fallback />
    );
  return <Route {...rest} render={routeComponent} />;
};

export default PrivateRoute;
