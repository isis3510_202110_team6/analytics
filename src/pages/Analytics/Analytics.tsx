import React, { useEffect, useState, useRef } from "react";
import {
  Container,
  Grid,
  Typography,
  Paper,
  TextField,
  Snackbar,
  Divider,
  Hidden,
  CircularProgress,
  Box,
  MenuItem,
  CircularProgressProps,
} from "@material-ui/core";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  ResponsiveContainer,
  LineChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Line,
  Label,
  ComposedChart,
  Bar,
  PieChart,
  Pie,
  Sector,
  Cell,
  Legend,
  BarChart,
} from "recharts";
import moment from "moment";
import { Autocomplete, Alert } from "@material-ui/lab";
import {matchSorter} from "match-sorter";
import FindCleverlynksTable from "./FindCleverlynksTable";
import { PFXOrder, QListCompanysQuery, QListOrdersQuery } from "../../models/Interfaces";
import { GraphQLResult } from "@aws-amplify/api";
import callGraphQL from "../../models/graphql-api";
import { getAllCompanies, privateAnalyticsQuery } from "../../models/Analytics/AnalyticsQueries";

export function compareMoments(a: moment.Moment, b: moment.Moment) {
    if (a.isSame(b)) return 0;
    return a.isBefore(b) ? -1 : 1;
  }
interface TimeData {
  time: number;
  value: number;
}

interface TimeDataAvg {
  time: number;
  value: number;
  avg: number;
}

interface NameData {
  name: string;
  value: number;
}

interface Companies {
  createdAt: any;
  name: string;
  id: string;
}

const COLORS = [
  "#e6194b",
  "#3cb44b",
  "#ffe119",
  "#4363d8",
  "#f58231",
  "#911eb4",
  "#46f0f0",
  "#f032e6",
  "#bcf60c",
  "#fabebe",
  "#008080",
  "#e6beff",
  "#9a6324",
  "#fffac8",
  "#800000",
  "#aaffc3",
  "#808000",
  "#ffd8b1",
  "#000075",
  "#808080",
  "#000000",
];
const AVGPERIOD = 14;
const TOTAL_ORDER_AMOUNT = 23000 + moment().diff(moment("2021-01-11"), "d") * 150;
const PAYMENT_METHODS = [
  "Efectivo",
  "Datafono",
  "Transferencia",
  "Nequi",
  "ePayCo",
  "Redeban",
  "Mercado Pago",
  "Selección",
];
export const TEST_COMPANIES = [
  "a15Q1X0j82",
  "GJFHC0De7J",
  "xx5StWvN8G",
  "3CBlOWIZWK1",
  "BjWmUhdjEc",
  "6KPHI8wzH6",
  "mJZWneHPVg",
  "F4y59BD9sv",
  "MStuhRROsi",
  "ypDlX861rU",
  "21y6pqPQ0V",
  "bMabE4qCxT",
  "9PLaGFvXiR",
  "EAIndLd1Hs",
  "BmTop2xJPS",
  "IMMEDSccIv",
  "Xohb1GMdnq",
  "5O1YcQlXSU",
  "0YGX1t3d8i",
  "FkFnJ2toLH",
  "CKFtmt34dF",
];
const TIMEFRAME_FILTERS = ["day", "week", "month", "quarter", "year"];
type TIMEFRAME_FILTERS_TYPE = "day" | "week" | "month" | "quarter" | "year";

const PrivateGrowthDashboard = () => {
  const [companies, setCompanies] = useState<Companies[]>([]);
  const [orders, setOrders] = useState<PFXOrder[]>([]);
  const [filter, setFilter] = useState<Companies[]>([]);
  const [paymentFilter, setPaymentFilter] = useState<string[]>([]);
  const [companyColors, setCompanyColors] = useState(new Map<string, string>());
  const [totalSales, setTotalSales] = useState<TimeData[]>([]);
  const [dailySales, setDailySales] = useState<TimeDataAvg[]>([]);
  const [totalOrders, setTotalOrders] = useState<TimeData[]>([]);
  const [dailyOrders, setDailyOrders] = useState<TimeDataAvg[]>([]);
  const [cumulativeAvgOrderPrice, setCumulativeAvgOrderPrice] = useState<TimeData[]>([]);
  const [rollingAvgOrderPrice, setRollingAvgOrderPrice] = useState<TimeData[]>([]);
  const [totalClients, setTotalClients] = useState<TimeData[]>([]);
  const [weeklyClients, setWeeklyClients] = useState<TimeDataAvg[]>([]);
  const [salesPerClient, setSalesPerClient] = useState<NameData[]>([]);
  const [ordersPerClient, setOrdersPerClient] = useState<NameData[]>([]);
  const [salesPerPaymentMethod, setSalesPerPaymentMethod] = useState<NameData[]>([]);
  const [ordersPerPaymentMethod, setOrdersPerPaymentMethod] = useState<NameData[]>([]);
  const [weeklySalesPerClient, setWeeklySalesPerClient] = useState<NameData[]>([]);
  const [weeklyOrdersPerClient, setWeeklyOrdersPerClient] = useState<NameData[]>([]);
  const [conversionRate, setConversionRate] = useState(0);
  const [renders, setRenders] = useState(0);
  const firstRender = useRef(true);
  const active = useRef(true);

  const [pieActiveIndexSales, setPieActiveIndexSales] = useState(0);
  const [pieActiveIndexOrders, setPieActiveIndexOrders] = useState(0);
  const [weeklyPieActiveIndexSales, setWeeklyPieActiveIndexSales] = useState(0);
  const [weeklyPieActiveIndexOrders, setWeeklyPieActiveIndexOrders] = useState(0);
  const [grouping, setGrouping] = useState<TIMEFRAME_FILTERS_TYPE>("day");
  const [loadingOrders, setLoadingOrders] = useState(true);
  const [loadingCompanies, setLoadingCompanies] = useState(true);
  const [ordersLoadingProgress, setOrdersLoadingProgress] = useState(0);
  const [snackbar, setSnackbar] = useState<{ message: string; type: "info" | "success" | "warning" | "error" } | null>(
    null
  );

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      Card: {
        [theme.breakpoints.up("md")]: {
          padding: 30,
          height: 500,
        },
        [theme.breakpoints.down("md")]: {
          paddingTop: 30,
          paddingBottom: 30,
          height: 400,
        },
        width: "100%",
        position: "relative",
      },
      PieCard: {
        [theme.breakpoints.up("md")]: {
          padding: 10,
          height: 500,
        },
        [theme.breakpoints.down("md")]: {
          paddingTop: 30,
          paddingBottom: 10,
          height: 400,
        },
        width: "100%",
        position: "relative",
      },
      divider: {
        height: 32,
      },
    })
  );

  const classes = useStyles();

  async function getOrders() {
    const ords: PFXOrder[] = [];
    let nextToken: string | null = null;

    do {
      try {
        const ans: GraphQLResult<QListOrdersQuery> = await callGraphQL<QListOrdersQuery>(
          privateAnalyticsQuery(nextToken)
        );
        ords.push(
          ...(ans.data!.qListOrders!.items! as PFXOrder[]).filter(
            o => o !== null && o.orderStatus! % 100 === 1 && !TEST_COMPANIES.includes(o.companyId!)
          )
        );
        setOrdersLoadingProgress(Math.min(99, Math.round((ords.length / TOTAL_ORDER_AMOUNT) * 100)));
        nextToken = ans.data!.qListOrders!.nextToken!;
      } catch (ans) {
        ords.push(
          ...(ans.data!.qListOrders!.items! as PFXOrder[]).filter(
            o => o !== null && o.orderStatus! % 100 === 1 && !TEST_COMPANIES.includes(o.companyId!)
          )
        );
        setOrdersLoadingProgress(Math.min(99, Math.round((ords.length / TOTAL_ORDER_AMOUNT) * 100)));
        nextToken = ans.data!.qListOrders!.nextToken;
      }
    } while (nextToken !== null && active.current);

    if (active.current) {
      ords.sort((a, b) => compareMoments(moment(a.createdAt), moment(b.createdAt)));
      setOrders(ords);
    }

    return ords;
  }

  async function getCompanies() {
    let comps: Companies[] = [];
    let nextToken: string | null = null;

    do {
      const ans: GraphQLResult<QListCompanysQuery> = await callGraphQL<QListCompanysQuery>(getAllCompanies(nextToken));
      comps.push(...(ans!.data!.qListCompanys!.items! as Companies[]));
      nextToken = ans.data!.qListCompanys!.nextToken!;
    } while (nextToken !== null && active.current);

    if (active.current) {
      comps = comps.filter(c => c !== null && !TEST_COMPANIES.includes(c.id!));
      setCompanies(comps);
    }

    return comps;
  }

  useEffect(() => {
    getOrders().then(data => {
      if (active.current) {
        getWeeks(data[0].createdAt, data[data.length - 1].createdAt);
        getTotalSales(data);
        getOrdersPerClient(data);
        getConversionRate(data);
        getPaymentMethods(data);
        setLoadingOrders(false);
      }
    });
    return () => {
      active.current = false;
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getCompanies().then(data => {
      if (active.current) {
        getTotalClients(data);
        setLoadingCompanies(false);
      }
    });
    return () => {
      active.current = false;
    };
  }, []);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }
    setLoadingOrders(true);
    setSnackbar(null);

    let newOrders = orders;
    if (filter.length > 0) newOrders = newOrders.filter(o => filter.some(c => o.companyId === c.id));
    if (paymentFilter.length > 0)
      newOrders = newOrders.filter(o =>
        paymentFilter.some(
          p => o.paymentMethod!.split(":")[0] === p || (o.paymentMethod! === "Credito" && p === "ePayCo")
        )
      );
    newOrders = newOrders.map(o => ({ ...o, createdAt: moment(o.createdAt).startOf(grouping).toISOString() }));

    if (newOrders.length >= 1) {
      getTotalSales(newOrders);
      getConversionRate(newOrders);
      getPaymentMethods(newOrders);
      if (filter.length !== 1) getOrdersPerClient(newOrders);
    } else setSnackbar({ type: "info", message: "No hay ordenes con el filtro elegido" });
    setLoadingOrders(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter, paymentFilter, grouping]);

  function getWeeks(first: any, last: any) {
    const res: number[] = [];
    const start = moment.unix(first ?? 0).endOf("week");
    const end = moment.unix(last ?? 0).endOf("week");

    while (start.isBefore(end)) {
      res.push(start.unix());
      start.add(1, "week");
    }

    return res;
  }

  function getTotalSales(orders: PFXOrder[]) {
    const acumSales: TimeData[] = [];
    const dSales: TimeData[] = [];
    const dSalesAvg: number[] = [];
    const acumOrders: TimeData[] = [];
    const dOrders: TimeData[] = [];
    const dOrdersAvg: number[] = [];
    const rollAvg: TimeData[] = [];

    const day = moment(orders[0].createdAt).startOf("day");
    let acumSal = 0,
      dSal = 0,
      acumOrd = 0,
      dOrd = 0;

    orders.forEach(o => {
      const currd = moment(o.createdAt).startOf("day");

      while (!day.isSame(currd)) {
        acumSales.push({ time: day.unix(), value: Math.round(acumSal) });
        dSales.push({ time: day.unix(), value: Math.round(dSal) });
        acumOrders.push({ time: day.unix(), value: acumOrd });
        dOrders.push({ time: day.unix(), value: dOrd });
        dOrd = 0;
        dSal = 0;
        day.add(1, grouping);
      }

      dSal += o.price!.total!;
      acumSal += o.price!.total!;
      dOrd++;
      acumOrd++;
    });

    const n = acumSales.length;
    for (let i = 0; i < n; ++i) {
      const p = Math.min(n - (i + 1), i, AVGPERIOD);
      rollAvg.push({
        time: acumSales[i].time,
        value: Math.round(
          (acumSales[i + p].value - (p + 1 > i ? 0 : acumSales[i - p - 1].value)) /
            (acumOrders[i + p].value - (p + 1 > i ? 0 : acumOrders[i - p - 1].value))
        ),
      });
      dSalesAvg.push(Math.round((acumSales[i + p].value - (p + 1 > i ? 0 : acumSales[i - p - 1].value)) / (p * 2 + 1)));
      dOrdersAvg.push(
        Math.round((acumOrders[i + p].value - (p + 1 > i ? 0 : acumOrders[i - p - 1].value)) / (p * 2 + 1))
      );
    }

    acumSales.push({ time: day.unix(), value: Math.round(acumSal) });
    dSales.push({ time: day.unix(), value: Math.round(dSal) });
    acumOrders.push({ time: day.unix(), value: acumOrd });
    dOrders.push({ time: day.unix(), value: dOrd });

    setDailySales(
      dSales.map((s, i) => ({ time: s.time, value: s.value, avg: dSalesAvg[i < dSalesAvg.length ? i : i - 1] }))
    );
    setTotalSales(acumSales);
    setTotalOrders(acumOrders);
    setDailyOrders(
      dOrders.map((s, i) => ({ time: s.time, value: s.value, avg: dOrdersAvg[i < dOrdersAvg.length ? i : i - 1] }))
    );
    setRollingAvgOrderPrice(rollAvg);
    setCumulativeAvgOrderPrice(
      acumSales.map((s, i) => ({ time: s.time, value: Math.round(s.value / acumOrders[i].value) }))
    );
  }

  function getTotalClients(allCompanies: Companies[]) {
    const m = new Map<number, number>();

    let wstart = Math.min(),
      wend = Math.max();
    allCompanies.forEach(c => {
      const currd = moment(c.createdAt).endOf("week").unix();
      wstart = Math.min(wstart, currd);
      wend = Math.max(wend, currd);
    });

    const start = moment.unix(wstart);
    const end = moment.unix(wend);
    while (start.isBefore(end)) {
      m.set(start.unix(), 0);
      start.add(1, "week");
    }

    allCompanies.forEach(c => {
      const currd = moment(c.createdAt).endOf("week").unix();
      const amount = m.get(currd) ?? 0;
      m.set(currd, amount + 1);
    });

    const dayToNewCompanies = Array.from(m.entries()).sort((a, b) => a[0] - b[0]);
    let acum = 0;
    const acumTimeData: TimeData[] = dayToNewCompanies.map(comp => {
      acum += comp[1];
      return { time: comp[0], value: acum };
    });

    setTotalClients(acumTimeData);
    setWeeklyClients(
      dayToNewCompanies.map((c, i) => {
        const p = Math.min(acumTimeData.length - (i + 1), i, Math.round(AVGPERIOD / 7));
        return {
          time: c[0],
          value: c[1],
          avg: +((acumTimeData[i + p].value - (p + 1 > i ? 0 : acumTimeData[i - p - 1].value)) / (p * 2 + 1)).toFixed(
            1
          ),
        };
      })
    );
  }

  function getOrdersPerClient(orders: PFXOrder[]) {
    const ms = new Map<string, number>();
    const mo = new Map<string, number>();
    const wms = new Map<string, number>();
    const wmo = new Map<string, number>();

    const wee = moment().subtract(7, "d");

    orders.forEach(o => {
      const key = o.company!.name!;
      const sal = ms.get(key) ?? 0;
      const ord = mo.get(key) ?? 0;
      ms.set(key, sal + o.price!.total!);
      mo.set(key, ord + 1);
      if (moment(o.createdAt).isSameOrAfter(wee)) {
        const wsal = wms.get(key) ?? 0;
        const word = wmo.get(key) ?? 0;
        wms.set(key, wsal + o.price!.total!);
        wmo.set(key, word + 1);
      }
    });

    const spc = Array.from(ms.entries())
      .sort((a, b) => b[1] - a[1])
      .splice(0, 10)
      .map(([name, value]) => ({ name, value: Math.round(value) }));
    const opc = Array.from(mo.entries())
      .sort((a, b) => b[1] - a[1])
      .splice(0, 10)
      .map(([name, value]) => ({ name, value: Math.round(value) }));
    const wspc = Array.from(wms.entries())
      .sort((a, b) => b[1] - a[1])
      .splice(0, 10)
      .map(([name, value]) => ({ name, value: Math.round(value) }));
    const wopc = Array.from(wmo.entries())
      .sort((a, b) => b[1] - a[1])
      .splice(0, 10)
      .map(([name, value]) => ({ name, value: Math.round(value) }));
    setSalesPerClient(spc);
    setOrdersPerClient(opc);
    setWeeklySalesPerClient(wspc);
    setWeeklyOrdersPerClient(wopc);

    const cc = new Map<string, string>();
    let i = 0;
    spc.forEach(c => {
      if (!cc.has(c.name)) cc.set(c.name, COLORS[i++ % COLORS.length]);
    });
    opc.forEach(c => {
      if (!cc.has(c.name)) cc.set(c.name, COLORS[i++ % COLORS.length]);
    });
    wspc.forEach(c => {
      if (!cc.has(c.name)) cc.set(c.name, COLORS[i++ % COLORS.length]);
    });
    wopc.forEach(c => {
      if (!cc.has(c.name)) cc.set(c.name, COLORS[i++ % COLORS.length]);
    });
    setCompanyColors(cc);
  }

  function getConversionRate(orders: PFXOrder[]) {
    let rend = 0;
    let substract = 0;
    const clevs = new Set<string>();

    orders.forEach(o => {
      if (!clevs.has(o.cleverlynkId!)) {
        const r = o.cleverlynk?.amountRenders;
        if (!r || r < 1) substract++;
        else {
          rend += r;
          clevs.add(o.cleverlynkId!);
        }
      }
    });

    setRenders(rend);
    setConversionRate(((orders.length - substract) / rend) * 100);
  }

  function getPaymentMethods(orders: PFXOrder[]) {
    const pmo = new Map<string, number>();
    const pms = new Map<string, number>();

    orders.forEach(o => {
      if (o.paymentMethod!.split(":")[0] === "Credito") o.paymentMethod = "ePayCo";
      const sal = pms.get(o.paymentMethod!.trim()) ?? 0;
      const ord = pmo.get(o.paymentMethod!.trim()) ?? 0;
      pms.set(o.paymentMethod!.trim(), sal + o.price!.total!);
      pmo.set(o.paymentMethod!.trim(), ord + 1);
    });

    setOrdersPerPaymentMethod(
      Array.from(pmo.entries())
        .sort((a, b) => a[1] - b[1])
        .map(([name, value]) => ({ name, value }))
    );

    setSalesPerPaymentMethod(
      Array.from(pms.entries())
        .sort((a, b) => a[1] - b[1])
        .map(([name, value]) => ({ name, value: Math.round(value) }))
    );
  }

  const renderFiltersAndTable = () => (
    <Grid item container xs={12} justify="center" spacing={2} style={{ marginTop: 24 }}>
      <Hidden smDown>
        <Grid item xs={6}>
          <Typography variant="h6" align="center" paragraph>
            Filtra las analíticas
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="h6" align="center" paragraph>
            Cleverlynks Activos
          </Typography>
        </Grid>
      </Hidden>
      <Grid item container xs={12} sm={6} spacing={1}>
        <Grid item xs={12}>
          <Autocomplete
            multiple
            style={{ backgroundColor: "#FFF" }}
            filterSelectedOptions
            fullWidth
            options={companies}
            getOptionLabel={comp => comp.name}
            filterOptions={(options, { inputValue }) => matchSorter(options, inputValue, { keys: ["name"] })}
            value={filter}
            onChange={(_, val: Companies[]) => setFilter(val)}
            renderInput={params => (
              <TextField
                {...params}
                label="Filter by companies"
                placeholder={filter.length > 0 ? "..." : "No filter"}
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Autocomplete
            multiple
            style={{ backgroundColor: "#FFF" }}
            filterSelectedOptions
            fullWidth
            options={PAYMENT_METHODS}
            filterOptions={(options, { inputValue }) => matchSorter(options, inputValue)}
            value={paymentFilter}
            onChange={(_, val) => setPaymentFilter(val)}
            renderInput={params => (
              <TextField
                {...params}
                label="Filter by Payment Method"
                placeholder={filter.length > 0 ? "..." : "No Filter"}
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            variant="outlined"
            label="Group by time frame"
            style={{ backgroundColor: "#FFF" }}
            select
            value={grouping}
            onChange={ev => setGrouping(ev.target.value as TIMEFRAME_FILTERS_TYPE)}
            fullWidth>
            {TIMEFRAME_FILTERS.map(t => (
              <MenuItem key={t} value={t}>
                {t.charAt(0).toUpperCase() + t.slice(1)}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={6}>
        <FindCleverlynksTable
          setSnackbar={(message: any, type: any) => setSnackbar({ message, type })}
          companies={filter.map(f => f.id)}
        />
      </Grid>
    </Grid>
  );

  return (
    <div style={{ backgroundColor: "#f7f7f7" }}>
      <Container maxWidth="xl">
        <Snackbar
          open={snackbar !== null}
          autoHideDuration={6000}
          onClose={() => setSnackbar(null)}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}>
          <Alert onClose={() => setSnackbar(null)} severity={snackbar?.type ?? "info"}>
            {snackbar?.message ?? ""}
          </Alert>
        </Snackbar>
        <Grid container spacing={2} justify="space-evenly">
          {renderFiltersAndTable()}
          <Grid item xs={10} style={{ marginBottom: 24 }}>
            <Divider />
          </Grid>
          <Grid item xs={12} md={4}>
            <Paper elevation={5} style={{ padding: 30 }}>
              <Typography variant="h5" align="center">
                Conversion Rate: {conversionRate.toFixed(2)}%
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} md={4}>
            <Paper elevation={5} style={{ padding: 30 }}>
              <Typography variant="h5" align="center">
                Total renders: {renders}
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} className={classes.divider} />
          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                Total sales
              </Typography>
              <ResponsiveContainer>
                <LineChart data={totalSales} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <CartesianGrid horizontal={false} />
                  <XAxis
                    dataKey="time"
                    type="number"
                    domain={["TimedataMin", "TimedataMax"]}
                    ticks={getWeeks(totalSales[0]?.time, totalSales[totalSales.length - 1]?.time)}
                    tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                    <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis unit="M" dataKey="value" tickFormatter={tick => String(tick / 1000000)}>
                    <Label value={"Ventas totales"} position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip
                    formatter={(value: any) => [currencyFormat(value as number), "Ventas"]}
                    labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                  />
                  <Line type="monotone" dataKey="value" stroke="#1D2C41" strokeWidth={2} dot={false} />
                </LineChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                {"Sales" + grouping.charAt(0).toUpperCase() + grouping.slice(1)}
              </Typography>
              <ResponsiveContainer>
                <ComposedChart data={dailySales} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis
                    dataKey="time"
                    type="number"
                    domain={["TimedataMin", "TimedataMax"]}
                    ticks={getWeeks(dailySales[0]?.time, dailySales[dailySales.length - 1]?.time)}
                    tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                    <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis unit="M" dataKey="value" tickFormatter={tick => String(tick / 1000000)}>
                    <Label value="Ventas nuevas" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip
                    formatter={(value: number, name: string) => [
                      currencyFormat(value as number),
                      name === "value" ? "Ventas" : "Promedio",
                    ]}
                    labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                  />
                  <CartesianGrid horizontal={false} />
                  <Bar dataKey="value" fill="#02F7BF" />
                  <Line type="monotone" dataKey="avg" stroke="#1D2C41" strokeWidth={2} dot={false} />
                </ComposedChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>

          <Grid item xs={12} className={classes.divider} />

          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                Number of orders
              </Typography>
              <ResponsiveContainer>
                <LineChart data={totalOrders} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis
                    dataKey="time"
                    type="number"
                    domain={["TimedataMin", "TimedataMax"]}
                    ticks={getWeeks(totalOrders[0]?.time, totalOrders[totalOrders.length - 1]?.time)}
                    tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                    <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis dataKey="value">
                    <Label value="Ordenes totales" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip
                    formatter={(value : any) => [value, "Ordenes"]}
                    labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                  />
                  <CartesianGrid horizontal={false} />
                  <Line type="monotone" dataKey="value" stroke="#1D2C41" strokeWidth={2} dot={false} />
                </LineChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                {"Orders" + grouping.charAt(0).toUpperCase() + grouping.slice(1)}
              </Typography>
              <ResponsiveContainer>
                <ComposedChart data={dailyOrders} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis
                    dataKey="time"
                    type="number"
                    domain={["TimedataMin", "TimedataMax"]}
                    ticks={getWeeks(dailyOrders[0]?.time, dailyOrders[dailyOrders.length - 1]?.time)}
                    tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                    <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis dataKey="value">
                    <Label value="Ordenes nuevas" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip
                    formatter={(value: any, name: string) => [value, name === "value" ? "Ordenes" : "Promedio"]}
                    labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                  />
                  <CartesianGrid horizontal={false} />
                  <Bar dataKey="value" fill="#02F7BF" />
                  <Line type="monotone" dataKey="avg" stroke="#1D2C41" strokeWidth={2} dot={false} />
                </ComposedChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>

          <Grid item xs={12} className={classes.divider} />

          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                Cumulative avg order
              </Typography>
              <ResponsiveContainer>
                <LineChart data={cumulativeAvgOrderPrice} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis
                    dataKey="time"
                    type="number"
                    domain={["TimedataMin", "TimedataMax"]}
                    ticks={getWeeks(
                      cumulativeAvgOrderPrice[0]?.time,
                      cumulativeAvgOrderPrice[cumulativeAvgOrderPrice.length - 1]?.time
                    )}
                    tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                    <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis dataKey="value" unit="K" tickFormatter={t => String(t / 1000)}>
                    <Label value="Media acumulada" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip
                    formatter={(value: any) => [currencyFormat(value as number), "Costo Promedio orden"]}
                    labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                  />
                  <CartesianGrid horizontal={false} />
                  <Line type="monotone" dataKey="value" stroke="#1D2C41" strokeWidth={2} dot={false} />
                </LineChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                Centered moving avg order ({AVGPERIOD} days around)
              </Typography>
              <ResponsiveContainer>
                <LineChart data={rollingAvgOrderPrice} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis
                    dataKey="time"
                    type="number"
                    domain={["TimedataMin", "TimedataMax"]}
                    ticks={getWeeks(
                      rollingAvgOrderPrice[0]?.time,
                      rollingAvgOrderPrice[rollingAvgOrderPrice.length - 1]?.time
                    )}
                    tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                    <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis dataKey="value" unit="K" tickFormatter={t => String(t / 1000)}>
                    <Label value="Media móvil" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip
                    formatter={(value: any) => [currencyFormat(value as number), "Costo Promedio orden"]}
                    labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                  />
                  <CartesianGrid horizontal={false} />
                  <Line type="monotone" dataKey="value" stroke="#1D2C41" strokeWidth={2} dot={false} />
                </LineChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>
          {filter.length === 0 && (
            <>
              <Grid item xs={12} className={classes.divider} />

              <Grid item xs={12} md={6}>
                <Paper elevation={5} className={classes.Card}>
                  {loadingCompanies && <CircularProgress />}
                  <Typography variant="h5" align="center">
                    Number of clients
                  </Typography>
                  <ResponsiveContainer>
                    <LineChart data={totalClients} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                      <XAxis
                        dataKey="time"
                        type="number"
                        domain={["TimedataMin", "TimedataMax"]}
                        ticks={getWeeks(totalClients[0]?.time, totalClients[totalClients.length - 1]?.time)}
                        tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                        <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                      </XAxis>
                      <YAxis dataKey="value">
                        <Label value="Clientes totales" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                      </YAxis>
                      <Tooltip
                        formatter={(value : any) => [value, "Clientes"]}
                        labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                      />
                      <CartesianGrid horizontal={false} />
                      <Line type="monotone" dataKey="value" stroke="#1D2C41" strokeWidth={2} dot={false} />
                    </LineChart>
                  </ResponsiveContainer>
                </Paper>
              </Grid>
              <Grid item xs={12} md={6}>
                <Paper elevation={5} className={classes.Card}>
                  {loadingCompanies && <CircularProgress />}
                  <Typography variant="h5" align="center">
                    New clients / Week
                  </Typography>
                  <ResponsiveContainer>
                    <ComposedChart data={weeklyClients} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                      <XAxis
                        dataKey="time"
                        type="number"
                        domain={["TimedataMin", "TimedataMax"]}
                        ticks={getWeeks(weeklyClients[0]?.time, weeklyClients[weeklyClients.length - 1]?.time)}
                        tickFormatter={tick => moment.unix(tick).format("DD MMM YY")}>
                        <Label value="Día" position="bottom" style={{ textAnchor: "middle" }} />
                      </XAxis>
                      <YAxis dataKey="value">
                        <Label value="Clientes nuevos" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                      </YAxis>
                      <Tooltip
                        formatter={(value: any, name: string) => [value, name === "value" ? "Clientes nuevos" : "Promedio"]}
                        labelFormatter={l => moment.unix(l as number).format("DD MMM YY")}
                      />
                      <CartesianGrid horizontal={false} />
                      <Bar dataKey="value" fill="#02F7BF" />
                      <Line type="monotone" dataKey="avg" stroke="#1D2C41" strokeWidth={2} dot={false} />
                    </ComposedChart>
                  </ResponsiveContainer>
                </Paper>
              </Grid>
            </>
          )}
          {filter.length !== 1 && (
            <>
              <Grid item xs={12} className={classes.divider} />

              <Grid item xs={12} md={6}>
                <Paper elevation={5} className={classes.PieCard}>
                  {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
                  <Typography variant="h5" align="center">
                    Top 10 Companies by sales
                  </Typography>
                  <ResponsiveContainer>
                    <PieChart margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                      <Pie
                        dataKey="value"
                        data={salesPerClient}
                        activeIndex={pieActiveIndexSales}
                        activeShape={renderActiveShapeSales}
                        innerRadius="40%"
                        outerRadius="70%"
                        paddingAngle={5}
                        onMouseEnter={(_: any, index: React.SetStateAction<number>) => setPieActiveIndexSales(index)}>
                        {salesPerClient.map(data => (
                          <Cell key={data.name} fill={companyColors.get(data.name)} />
                        ))}
                      </Pie>
                      <Legend verticalAlign="bottom" />
                    </PieChart>
                  </ResponsiveContainer>
                </Paper>
              </Grid>
              <Grid item xs={12} md={6}>
                <Paper elevation={5} className={classes.PieCard}>
                  {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
                  <Typography variant="h5" align="center">
                    Top 10 Companies by orders
                  </Typography>
                  <ResponsiveContainer>
                    <PieChart margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                      <Pie
                        dataKey="value"
                        data={ordersPerClient}
                        activeIndex={pieActiveIndexOrders}
                        activeShape={renderActiveShapeOrders}
                        innerRadius="40%"
                        outerRadius="70%"
                        paddingAngle={5}
                        onMouseEnter={(_: any, index: React.SetStateAction<number>) => setPieActiveIndexOrders(index)}>
                        {ordersPerClient.map((data, index) => (
                          <Cell key={data.name} fill={companyColors.get(data.name)} />
                        ))}
                      </Pie>
                      <Legend verticalAlign="bottom" />
                    </PieChart>
                  </ResponsiveContainer>
                </Paper>
              </Grid>

              <Grid item xs={12} className={classes.divider} />

              <Grid item xs={12} md={6}>
                <Paper elevation={5} className={classes.PieCard}>
                  {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
                  <Typography variant="h5" align="center">
                    Last 7 days Top 10 Companies by sales
                  </Typography>
                  <ResponsiveContainer>
                    <PieChart margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                      <Pie
                        dataKey="value"
                        data={weeklySalesPerClient}
                        activeIndex={weeklyPieActiveIndexSales}
                        activeShape={renderActiveShapeSales}
                        innerRadius="40%"
                        outerRadius="70%"
                        paddingAngle={5}
                        onMouseEnter={(_: any, index: React.SetStateAction<number>) => setWeeklyPieActiveIndexSales(index)}>
                        {weeklySalesPerClient.map(data => (
                          <Cell key={data.name} fill={companyColors.get(data.name)} />
                        ))}
                      </Pie>
                      <Legend verticalAlign="bottom" />
                    </PieChart>
                  </ResponsiveContainer>
                </Paper>
              </Grid>
              <Grid item xs={12} md={6}>
                <Paper elevation={5} className={classes.PieCard}>
                  {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
                  <Typography variant="h5" align="center">
                    Last 7 days Top 10 Companies by orders
                  </Typography>
                  <ResponsiveContainer>
                    <PieChart margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                      <Pie
                        dataKey="value"
                        data={weeklyOrdersPerClient}
                        activeIndex={weeklyPieActiveIndexOrders}
                        activeShape={renderActiveShapeOrders}
                        innerRadius="40%"
                        outerRadius="70%"
                        paddingAngle={5}
                        onMouseEnter={(_: any, index: React.SetStateAction<number>) => setWeeklyPieActiveIndexOrders(index)}>
                        {weeklyOrdersPerClient.map((data, index) => (
                          <Cell key={data.name} fill={companyColors.get(data.name)} />
                        ))}
                      </Pie>
                      <Legend verticalAlign="bottom" />
                    </PieChart>
                  </ResponsiveContainer>
                </Paper>
              </Grid>
            </>
          )}
          <Grid item xs={12} className={classes.divider} />

          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                Orders per payment method
              </Typography>
              <ResponsiveContainer>
                <BarChart data={ordersPerPaymentMethod} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis dataKey="name">
                    <Label value="Método de pago" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis dataKey="value">
                    <Label value="Ordenes totales" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip formatter={(value: any) => [value, "Ordenes"]} />
                  <Bar dataKey="value" fill="#02F7BF" />
                </BarChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper elevation={5} className={classes.Card}>
              {loadingOrders && <CircularProgressWithLabel value={ordersLoadingProgress} />}
              <Typography variant="h5" align="center">
                Sales per payment method
              </Typography>
              <ResponsiveContainer>
                <BarChart data={salesPerPaymentMethod} margin={{ top: 20, right: 20, left: 20, bottom: 50 }}>
                  <XAxis dataKey="name">
                    <Label value="Método de pago" position="bottom" style={{ textAnchor: "middle" }} />
                  </XAxis>
                  <YAxis unit="M" dataKey="value" tickFormatter={tick => String(tick / 1000000)}>
                    <Label value="Ventas totales" position="left" angle={-90} style={{ textAnchor: "middle" }} />
                  </YAxis>
                  <Tooltip formatter={(value: number) => [currencyFormat(value as number), "Ventas"]} />
                  <Bar dataKey="value" fill="#02F7BF" />
                </BarChart>
              </ResponsiveContainer>
            </Paper>
          </Grid>

          <Grid item xs={12} className={classes.divider} />
        </Grid>
      </Container>
    </div>
  );
};

const currencyFormat = (price: string | number) => {
  if (typeof price === "string") price = parseInt(price);
  if (isNaN(price)) return "";
  return new Intl.NumberFormat("es-CO", {
    style: "currency",
    currency: "COP",
    minimumFractionDigits: 0,
    useGrouping: true,
  }).format(price);
};

const renderActiveShapeSales = (props: { cx: any; cy: any; midAngle: any; innerRadius: any; outerRadius: any; startAngle: any; endAngle: any; fill: any; payload: any; percent: any; value: any; }) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 20) * cos;
  const my = cy + (outerRadius + 20) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 11;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 3}
        outerRadius={outerRadius + 6}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`Ventas ${currencyFormat(
        value
      )}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

const renderActiveShapeOrders = (props: { cx: any; cy: any; midAngle: any; innerRadius: any; outerRadius: any; startAngle: any; endAngle: any; fill: any; payload: any; percent: any; value: any; }) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 20) * cos;
  const my = cy + (outerRadius + 20) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 11;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 3}
        outerRadius={outerRadius + 6}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`Ventas ${value}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

function CircularProgressWithLabel(props: JSX.IntrinsicAttributes & CircularProgressProps) {
  return (
    <Box style={{ position: "absolute", top: "50%", left: "50%" }}>
      <CircularProgress disableShrink {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center">
        <Typography variant="caption" component="div" color="textSecondary">{`${Math.round((props.value as number))}%`}</Typography>
      </Box>
    </Box>
  );
}

export default PrivateGrowthDashboard;
