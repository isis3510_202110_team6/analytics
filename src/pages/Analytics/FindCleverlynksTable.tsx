import React, { useState, useEffect, useRef } from "react";
import {
  Typography,
  Paper,
  TableContainer,
  Table,
  TableRow,
  TableHead,
  TableCell,
  TableBody,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import {Link} from "@material-ui/icons";
import CopyToClipboard from "react-copy-to-clipboard";
import { PFXCleverlynk, QListCleverlynksQuery } from "../../models/Interfaces";
import { getAllCleverlynks } from "../../models/Analytics/AnalyticsQueries";
import { GraphQLResult } from "@aws-amplify/api";
import callGraphQL from "../../models/graphql-api";
import { TEST_COMPANIES } from "./Analytics";
import moment from "moment";
import { compareMoments } from "./Analytics";

interface FindCleverlynkspTableProps {
  setSnackbar: (message: string, type: "info" | "success" | "warning" | "error") => void;
  companies: string[];
}

function FindCleverlynksTable(props: FindCleverlynkspTableProps) {
  const [allCleverlynks, setAllCleverlynks] = useState<PFXCleverlynk[]>([]);
  const [cleverlynks, setCleverlynks] = useState<PFXCleverlynk[]>([]);
  const [loading, setLoading] = useState(true);
  const active = useRef(true);

  useEffect(() => {
    async function getCleverlynks() {
      let clevs: PFXCleverlynk[] = [];
      let nextToken: string | null = null;

      do {
        const ans: GraphQLResult<QListCleverlynksQuery> = await callGraphQL<QListCleverlynksQuery>(
          getAllCleverlynks(nextToken)
        );
        clevs.push(...(ans!.data!.qListCleverlynks!.items as PFXCleverlynk[]));
        nextToken = ans.data!.qListCleverlynks!.nextToken || null;
      } while (nextToken !== null && active.current);

      if (active.current) {
        clevs = clevs.filter(c => c !== null && !TEST_COMPANIES.includes(c.company!.id!));
        clevs.sort((a, b) => compareMoments(moment(b.updatedAt), moment(a.updatedAt)));
        setAllCleverlynks(clevs);
        setLoading(false);
      }

      return clevs;
    }

    getCleverlynks();
    return () => {
      active.current = false;
    };
  }, []);

  useEffect(() => {
    if (props.companies.length) setCleverlynks(allCleverlynks.filter(c => props.companies.includes(c.company!.id!)));
    else setCleverlynks(allCleverlynks);
  }, [allCleverlynks, props.companies]);

  return (
    <>
      <TableContainer component={Paper} style={{ maxHeight: 180, height: 180, position: "relative" }}>
        {loading && <CircularProgress style={{ position: "absolute", top: "50%", left: "50%" }} />}
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant="subtitle2" color="primary">
                  Cleverlynk
                </Typography>
              </TableCell>
              <TableCell>
                <Typography variant="subtitle2" color="primary">
                  Compañía
                </Typography>
              </TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {cleverlynks.map(row => (
              <TableRow
                key={row.id}
                hover
                component="a"
                href={"https://" + encodeURI(encodeCompany(row.company?.name) + ".clynk.me/" + row.id)}
                target="_blank">
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.company?.name}</TableCell>
                <TableCell align="right">
                  <CopyToClipboard
                    text={"https://" + encodeURI(encodeCompany(row.company?.name) + ".clynk.me/" + row.id)}>
                    <IconButton
                      style={{ padding: 0 }}
                      onClick={ev => {
                        ev.preventDefault();
                        props.setSnackbar("Link al Cleverlynk copiado al portapapeles!", "success");
                      }}
                      color="primary">
                      <Link fontSize="small" style={{ color: "#00B2FF" }} />
                    </IconButton>
                  </CopyToClipboard>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

const encodeCompany = (c: string | undefined) => {
  return c?.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match: string, index: number) => {
    if (+match === 0) return "";
    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
};

export default FindCleverlynksTable;
