#!/bin/bash
# amplify headless config:
set -e
IFS='|'

REACTCONFIG="{\
\"SourceDir\":\"src\",\
\"DistributionDir\":\"build\",\
\"BuildCommand\":\"npm run build && npx precompress build\",\
\"StartCommand\":\"npm start\"\
}"

AWSCLOUDFORMATIONCONFIG="{\
\"configLevel\":\"project\",\
\"useProfile\":true,\
\"profileName\":\"default\"
}"

FRONTEND="{\
\"frontend\":\"javascript\",\
\"framework\":\"react\",\
\"config\":$REACTCONFIG\
}"

PROVIDERS="{\
\"awscloudformation\":$AWSCLOUDFORMATIONCONFIG\
}"

echo Choose environment \(dev / prod \)
read env

echo Only bring schema\? \(y / n\)
read onlyschema

if [ "$env" = "prod" ]; then
  echo Updating schema from PROD
  aws appsync get-introspection-schema --api-id yxo32lc2k5aa3dkmxstw3qg6ke --format SDL --include-directives schema.graphql
else 
  echo Updating schema from DEV
  aws appsync get-introspection-schema --api-id ybg2v5fxtve4fn6hvck4f2hwym --format SDL --include-directives schema.graphql
fi

echo Creating Interface file
cd src/models 
node InterfaceScript.js
cd ../../

if [ "$onlyschema" = "y" ]; then
  exit 1
fi

rm -f ./src/API.ts
rm -rf ./src/graphql

echo Executing amplify pull
if [ "$env" = "prod" ]; then
  # bash get_env_vars.sh prod
  AMPLIFY="{\
  \"projectName\":\"cleverlynk\",\
  \"appId\":\"d3bf99gta8trsz\",\
  \"envName\":\"prod\",\
  \"defaultEditor\":\"code\"\
  }"
  
  echo n | amplify pull \
  --amplify $AMPLIFY \
  --frontend $FRONTEND \
  --providers $PROVIDERS \
  --no
else
  # bash get_env_vars.sh test
  AMPLIFY="{\
  \"projectName\":\"cleverlynk\",\
  \"appId\":\"d3bf99gta8trsz\",\
  \"envName\":\"dev\",\
  \"defaultEditor\":\"code\"\
  }"
  
  echo n | amplify pull \
  --amplify $AMPLIFY \
  --frontend $FRONTEND \
  --providers $PROVIDERS \
  --no
fi

echo Generating Amplify Code.
amplify codegen --max-depth 1

echo executing QueryTypeScript 
cd ./src/models
node QueryTypeScript.js

cd ./../..
echo deleting API and graphql
rm -rf ./src/API.ts
rm -rf ./src/graphql

exit
