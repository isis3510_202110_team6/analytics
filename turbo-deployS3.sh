s3url="s3://admin.cleverlynk.com"
echo "Will delete old build"
sudo rm -rf build
echo "Deleted old build"
echo "Make sure dependencies are installed"
npm i
echo "Installed dependencies"
echo "Will create new build"
npm run build
echo "New build ready"
echo "Will clean S3"
aws s3 rm $s3url --recursive
echo "Will deploy in S3"
aws s3 cp ./build $s3url --recursive --acl public-read
echo "Finished new deployments"